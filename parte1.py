from ast import While
import ast

from unittest import result
from xml.sax.handler import property_declaration_handler
import pygame 
import random
import pygame.freetype
FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.5

GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_PLAYING_2=3
GAME_STATE_PLAYING_RETRY=4
GAME_STATE_MENU_PERDIDA=5
GAME_STATE_EXIT=6

SPAWN_ASTEROIDE_TIME=500

#contador de puntuacion 
'''player'''
def create_player():
    player={
        'estatico':pygame.image.load('images/ship1.png'),
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-70,
        'y':600-71,
        'points':0,
        'vida':15
    }
    player['sprites']=player['estatico']
    return player

def draw_player(screen, player):
    sprite=player['sprites']
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)
    proyectil_sprite=pygame.image.load('images/projectil.png')

def move_player(player, delta):
    moved=False
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and player['x']>0:
        player['x']=max(player['x']-vel, 0)
        player['sprites']=player['estatico']
        moved=True
        player['animation_time']
    if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-70:
        player['x']=min(player['x']+vel, WINDOW_SIZE_X-70)
        player['sprites']=player['estatico']
        moved=True
    if keys[pygame.K_UP] and player['y']>0:
        player['y']=max(player['y']-vel, 0)
        player['sprites']=player['estatico']
        moved=True
    if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-71:
        player['y']=min(player['y']+vel, WINDOW_SIZE_Y-71)
        player['sprites']=player['estatico']
        moved=True
    if moved:
        player['animation_time']+=delta
        if player['animation_time']>400:
            player['animation_time']-=400
            player['sprite_index']=(player['sprite_index']+1)%1
    return player['x'],player['y']

def create_player2():
    player={
        'estatico':pygame.image.load('images/ship2.png'),
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-70,
        'y':600-71,
        'points':0,
        'vida':10
    }
    player['sprites']=player['estatico']
    return player

def draw_player2(screen, player):
    sprite=player['sprites']
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)


'''asteroides'''

def create_asteroides(asteroides):
    asteoride_sprite=pygame.image.load('images/asteroid.png')
    asteroide={'sprite': pygame.image.load('images/asteroid.png'),
                'time':500,
                'daño':1,

                'x': random.randint(0, WINDOW_SIZE_X-asteoride_sprite.get_width()),
                'y':0,
                'points':1,
                'explosion':pygame.image.load('images/explosion.png'),
                'explosion_nave':pygame.image.load('images/explosion_nav.png')
                }
    asteroides.append(asteroide)
    return asteroide

def create_asteroides2(asteroides):
    asteoride_sprite=pygame.image.load('images/asteroide2.png')
    asteroide={'sprite': pygame.image.load('images/asteroide2.png'),
                'time':500,
                'daño':1,

                'x': random.randint(0, WINDOW_SIZE_X-asteoride_sprite.get_width()),
                'y':0,
                'points':1,
                'explosion':pygame.image.load('images/explosion.png'),
                'explosion_nave':pygame.image.load('images/explosion_nav.png')
                }
    asteroides.append(asteroide)
    return asteroide

def borrar_asteroide(asteroides,player):
    for n in range(len(asteroides)-1, -1, -1):
        asteroide=asteroides[n]
        if asteroide['y']>800:
            del asteroides[n]
            player['vida']-=1

def move_asteroide(asteroides):
   for asteroide in asteroides:
        asteroide['y']+=2

def move_asteroide2(asteroides):
   for asteroide in asteroides:
        asteroide['y']+=3


def draw_asteroide(screen,asteroides):
    for asteroide in asteroides:
        sprite=asteroide['sprite']
        square=sprite.get_rect().move(asteroide['x'],asteroide['y'])
        screen.blit(sprite,square)

'''proyectiles'''
def create_proyectil(x,y):
    proyectil={'sprite':pygame.image.load('images/projectil.png'),
                'daño':1,
                'animation_time':100,
                'x':x+35,
                'y':y+35.5,
                'moved':False
    }
    return proyectil

def move_proyectil(proyectiles):
    delta=1500
 
    for proyectil in proyectiles:
        proyectil['y']=proyectil['y']-15
        proyectil['moved']=True
        if proyectil['moved']:
            proyectil['animation_time']-=delta
            proyectil['y']=proyectil['y']-15


def draw_proyectil(screen,proyectiles):
    for proyectil in proyectiles:
        sprite=proyectil['sprite']
        square=sprite.get_rect().move(proyectil['x'],proyectil['y'])
        screen.blit(sprite,square)


'''powerup'''
def create_iconoPU():
    pu={ 
        'sprite':pygame.image.load('images/powerup.png'),
        'activo':True,
        'x':random.randrange(0,WINDOW_SIZE_X-35),
        'y':random.randrange(0,WINDOW_SIZE_Y-35)
    }
    return pu

def draw_icon_poweup(screen, iconoUP ):
    print(iconoUP)
    for icono in iconoUP:
        sprite=icono['sprite']
        square=sprite.get_rect().move(icono['x'],icono['y'])
        screen.blit(sprite,square)
        #iconoUP.append(icono)
        
        
def move_powerup(proyectiles_powerup):
    delta=1500
    for proyectil in proyectiles_powerup:
        proyectil['y']=proyectil['y']-15
        proyectil['moved']=True
        if proyectil['moved']:
            proyectil['animation_time']-=delta
            proyectil['y']=proyectil['y']-15


def create_poyectiles_powerup(x,y):
    proyectil_izquierdo={
                'sprite':pygame.image.load('images/projectil.png'),
                'daño':1,
                'animation_time':100,
                'x':x+10,
                'y':y+35.5,
                'moved':False
            }
    proyectil_derecho={
        'sprite':pygame.image.load('images/projectil.png'),
                'daño':1,
                'animation_time':100,
                'x':x+54,
                'y':y+35.5,
                'moved':False
    }
    return proyectil_izquierdo,proyectil_derecho

def create_poyectiles_powerup2(x,y):
    proyectil_izquierdo={
                'sprite':pygame.image.load('images/3.png'),
                'daño':1,
                'animation_time':100,
                'x':x+5,
                'y':y+35.5,
                'moved':False
            }
    proyectil_derecho={
        'sprite':pygame.image.load('images/3.png'),
                'daño':1,
                'animation_time':100,
                'x':x+58,
                'y':y+35.5,
                'moved':False
    }
    return proyectil_izquierdo,proyectil_derecho
def draw_powerup(screen,proyectiles_powerup):
    for proyectil in proyectiles_powerup:
        sprite=proyectil['sprite']
        square=sprite.get_rect().move(proyectil['x'],proyectil['y'])
        screen.blit(sprite,square)

'''colisiones'''
def colisiones(proyectiles,asteroides,player,screen,sonido_explosion,sonido):
    for proyectil in proyectiles:
        sprite=proyectil['sprite']
        square=sprite.get_rect().move(proyectil['x'], proyectil['y'])
        for asteroide in asteroides:
            explosion=asteroide['explosion']
            asteroide_square=asteroide['sprite'].get_rect().move(asteroide['x'], asteroide['y'])
            square_explosio=sprite.get_rect().move(asteroide['x']-25, asteroide['y']-30)
            if square.colliderect(asteroide_square):
                if sonido==True:
                    sonido_explosion.play()
                player['points']+=asteroide['points']
                asteroide['time']=0
                asteroides.remove(asteroide)
                proyectiles.remove(proyectil)
                screen.blit(explosion,square_explosio)
                return proyectiles

def colisiones_jugador(asteroides,player,screen,proyectiles_powerup):
    sprite=player['sprites']
    square=sprite.get_rect().move(player['x'], player['y'])
    for asteroide in asteroides:
        sprite_asteroide=asteroide['sprite']
        explosion=asteroide['explosion_nave']
        square_asteroide=sprite_asteroide.get_rect().move(asteroide['x'],asteroide['y'])
        square_explosion=sprite.get_rect().move(asteroide['x']-25,asteroide['y']-30)
        if square.colliderect(square_asteroide):
            player['vida']-=asteroide['daño']
            asteroides.remove(asteroide)

            screen.blit(explosion,square_explosion)
            return player
def colision_icono_player(iconoUP,player,powerup,sonido_powerup,sonido):
    sprite=player['sprites']
    square=sprite.get_rect().move(player['x'],player['y'])
    for icono in iconoUP:
        sprite_i=icono['sprite']
        square_i=sprite_i.get_rect().move(icono['x'],icono['y'])
        if square.colliderect(square_i):
            if sonido==True:
                sonido_powerup.play()
            icono['activo']=False
            powerup=True
            
    for n in range(len(iconoUP)-1, -1, -1):
        if iconoUP[n]['activo']==False:
            del iconoUP[n]
    return powerup




def colisiones_powerup(proyectiles_powerup,asteroides,player,screen):
    for proyectil in proyectiles_powerup:
        sprite=proyectil['sprite']
        square=sprite.get_rect().move(proyectil['x'], proyectil['y'])
        for asteroide in asteroides:
            explosion=asteroide['explosion']
            asteroide_square=asteroide['sprite'].get_rect().move(asteroide['x'], asteroide['y'])
            square_explosio=sprite.get_rect().move(asteroide['x']-25, asteroide['y']-30)
            if square.colliderect(asteroide_square):
                player['points']+=asteroide['points']
                asteroide['time']=0
                asteroides.remove(asteroide)
                proyectiles_powerup.remove(proyectil)
                screen.blit(explosion,square_explosio)
                return proyectiles_powerup
'''menus'''

def menu(screen):
    start_btn_light=pygame.image.load('images/start.png')
    start_tbn_dark=pygame.image.load('images/start_dark.png')
    exit_btn_light=pygame.image.load('images/exit.png')
    exit_tbn_dark=pygame.image.load('images/exit_dark.png')
    background=pygame.image.load('images/fondomenu.jpg')
    start_btn=start_btn_light
    exit_btn=exit_btn_light
    going=True
    while going:
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                going=False
                resultado=GAME_STATE_EXIT
            if event.type== pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(50,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(450,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_EXIT
            square=start_btn.get_rect().move(50,300)
            if square.collidepoint(pygame.mouse.get_pos()):
                start_btn=start_tbn_dark
            else:
                start_btn=start_btn_light
            square=exit_btn.get_rect().move(450,300)
            if square.collidepoint(pygame.mouse.get_pos()):
                exit_btn=exit_tbn_dark
            else:
                exit_btn=exit_btn_light
            screen.blit(background, background.get_rect())
            screen.blit(start_btn, start_btn.get_rect().move(50,300))
            screen.blit(exit_btn, exit_btn.get_rect().move(450,300))
            pygame.display.flip()
    return resultado
            
def menu_perdida(screen):
    retry=pygame.image.load('images/retry.jpg')
    exit=pygame.image.load('images/exit.jpg')
    background=pygame.image.load('images/game_over_menu.png')
    resultado=GAME_STATE_MENU_PERDIDA
    going=True
    while going:
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                going=False
                resultado=GAME_STATE_EXIT
            if event.type== pygame.MOUSEBUTTONDOWN:
                square=retry.get_rect().move(150,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_PLAYING
                square=exit.get_rect().move(450,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_EXIT
            screen.blit(background, background.get_rect())
            screen.blit(retry, retry.get_rect().move(150,300))
            screen.blit(exit, exit.get_rect().move(450,300))
            pygame.display.flip()
    return resultado

def menu_victoria_final(screen):
    retry=pygame.image.load('images/retry.jpg')
    next=pygame.image.load('images/next.jpg')
    volver=pygame.image.load('images/volver_menu.jpg')
    exit=pygame.image.load('images/exit.jpg')
    background=pygame.image.load('images/sky.png')
    resultado=GAME_STATE_MENU_PERDIDA
    going=True
    while going:
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                going=False
                resultado=GAME_STATE_EXIT
            if event.type== pygame.MOUSEBUTTONDOWN:
                square=retry.get_rect().move(50,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_PLAYING_2
                square=exit.get_rect().move(370,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_EXIT
                square=volver.get_rect().move(650,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_MENU
            screen.blit(background, background.get_rect())
            screen.blit(retry, retry.get_rect().move(50,300))
            screen.blit(exit, exit.get_rect().move(370,300))
            screen.blit(volver, volver.get_rect().move(650,300))

            pygame.display.flip()
    return resultado

def menu_victory(screen):
    retry=pygame.image.load('images/retry.jpg')
    next=pygame.image.load('images/next.jpg')
    volver=pygame.image.load('images/volver_menu.jpg')
    exit=pygame.image.load('images/exit.jpg')
    background=pygame.image.load('images/menu_victoria.png')
    resultado=GAME_STATE_MENU_PERDIDA
    going=True
    while going:
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                going=False
                resultado=GAME_STATE_EXIT
            if event.type== pygame.MOUSEBUTTONDOWN:
                square=next.get_rect().move(150,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_PLAYING_2
                square=exit.get_rect().move(450,300)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    resultado=GAME_STATE_EXIT
            screen.blit(background, background.get_rect())
            screen.blit(next, next.get_rect().move(150,300))
            screen.blit(exit, exit.get_rect().move(450,300))
            pygame.display.flip()
    return resultado

'''finalizacion'''
def draw_overlay(screen, font, player,fallos):
    font.render_to(screen, (550, 550), 'Puntuació: '+str(player['points']))
    font.render_to(screen,(50,550), 'vidas:'+str(player['vida']))

def finalizar(asteroides,fallos,player):
    for asteroide in asteroides:
        if asteroide['y']==600:
            player['vida']-=1

'''niveles'''

def nivel1(screen,game_state):
    game_icon = pygame.image.load('images/ship1.png')
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    background = pygame.image.load('images/fondo.jpg').convert()
    pygame.display.set_caption('Maya space')
    pygame.display.set_icon(game_icon)
    sonido_explosion = pygame.mixer.Sound('sonido/explo.wav')
    sonido_proyectil =pygame.mixer.Sound('sonido/laser1.wav')
    sonido_powerup = pygame.mixer.Sound('sonido/powerup.wav')
    clock = pygame.time.Clock()
    player=create_player()
    asteroides=[]
    proyectiles=[]
    proyectiles_powerup=[]
    iconoPU=[]
    powerup=False
    sonido=True
    cont=25
    fallos=10
    cont_poweup=200
    game_state=GAME_STATE_PLAYING
    while game_state==GAME_STATE_PLAYING:
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                game_state=False
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_SPACE:
                    if sonido==True:
                        sonido_proyectil.play()
                    proyectil=create_proyectil(x,y)
                    proyectiles.append(proyectil)
                    if powerup:
                        proyectil1,proyectil2=create_poyectiles_powerup(x,y)
                        proyectiles_powerup.append(proyectil1)
                        proyectiles_powerup.append(proyectil2)
                if event.key==pygame.K_COMMA:
                    sonido=False
        screen.blit(background, background.get_rect())
        x,y=move_player(player, delta)
        draw_player(screen, player)
        if cont==20 :
            create_asteroides(asteroides)
        move_asteroide(asteroides)
        draw_asteroide(screen,asteroides)
        proyectil=create_proyectil(x,y)
        delta=1500
        move_proyectil(proyectiles)
        draw_proyectil(screen,proyectiles)
        move_powerup(proyectiles_powerup)
        draw_powerup(screen,proyectiles_powerup)
        if len(asteroides)>=1:
            colisiones(proyectiles,asteroides,player,screen,sonido_explosion,sonido)
            #player=colisiones_jugador(asteroides,player,screen,proyectiles_powerup)
            colisiones_powerup(proyectiles_powerup,asteroides,player,screen)
            finalizar(asteroides,fallos,player)
            draw_overlay(screen, overlay_font, player,fallos)
        if cont_poweup==1000:
            icono=create_iconoPU()
            iconoPU.append(icono)
        cont_poweup-=1
        draw_icon_poweup(screen, iconoPU )
        powerup=colision_icono_player(iconoPU,player,powerup,sonido_powerup,sonido) 
        cont-=1
        if cont==0:
            cont=20
        if cont_poweup==0:
            cont_poweup=1000
        
        #player=finalizar(player,asteroides)
        pygame.display.flip()
        borrar_asteroide(asteroides,player)
        if player['vida']==0:
            game_state=menu_perdida(screen)
            if game_state==GAME_STATE_PLAYING:
                clock = pygame.time.Clock()
                player=create_player()
                asteroides=[]
                proyectiles=[]
                proyectiles_powerup=[]
                iconoPU=[]
                powerup=False
                cont=25
                fallos=10
                cont_poweup=1000 
        if len(proyectiles_powerup)>90:
            powerup=False
            proyectiles_powerup=[]
            #game_state=draw_opciones_perdida(opcion,screen,game_state)
        if player['points']>=50:
            game_state=menu_victory(screen)
    return game_state

def nivel2(screen,game_state):
    game_icon = pygame.image.load('images/ship2.png')
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    sonido_explosion = pygame.mixer.Sound('sonido/explo.wav')
    sonido_proyectil =pygame.mixer.Sound('sonido/laser1.wav')
    sonido_powerup= pygame.mixer.Sound('sonido/powerup.wav')
    background = pygame.image.load('images/fondo2.png').convert()
    player=create_player2()
    asteroides=[]
    proyectiles=[]
    proyectiles_powerup=[]
    iconoPU=[]
    powerup=False
    cont=25
    fallos=10
    cont_poweup=100
    sonido=True
    pygame.display.set_caption('Maya space')
    pygame.display.set_icon(game_icon)

    clock = pygame.time.Clock()
    elapsed_time=10000
    game_state=GAME_STATE_PLAYING_2
    while game_state ==GAME_STATE_PLAYING_2:
        screen.blit(background, background.get_rect())
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type== pygame.QUIT:
                game_state=False
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_SPACE:
                    if sonido==True: 
                        sonido_proyectil.play()
                    x,y=move_player(player, delta)
                    proyectil=create_proyectil(x,y)
                    proyectiles.append(proyectil)
                    if powerup:
                        proyectil1,proyectil2=create_poyectiles_powerup2(x,y)
                        proyectiles_powerup.append(proyectil1)
                        proyectiles_powerup.append(proyectil2)
                if event.key==pygame.K_COMMA:
                    sonido=False
        
        draw_player2(screen, player)
        if cont==20 :
            create_asteroides2(asteroides)
        move_asteroide2(asteroides)
        draw_asteroide(screen,asteroides)
        x,y=move_player(player, delta)
        proyectil=create_proyectil(x,y)
        keys = pygame.key.get_pressed()
        delta=1500

        move_proyectil(proyectiles)
        draw_proyectil(screen,proyectiles)
        move_powerup(proyectiles_powerup)
        draw_powerup(screen,proyectiles_powerup)

        if len(asteroides)>=1:
            colisiones(proyectiles,asteroides,player,screen,sonido_explosion,sonido)
            #player=colisiones_jugador(asteroides,player,screen,proyectiles_powerup)
            colisiones_powerup(proyectiles_powerup,asteroides,player,screen)
            finalizar(asteroides,fallos,player)
            draw_overlay(screen, overlay_font, player,fallos)
        if cont_poweup==1000:
            icono=create_iconoPU()
            iconoPU.append(icono)
        cont_poweup-=1
        draw_icon_poweup(screen, iconoPU )
        powerup=colision_icono_player(iconoPU,player,powerup,sonido_powerup,sonido) 
            

        cont-=1
        if cont==0:
            cont=20
        if cont_poweup==0:
            cont_poweup=1000
        if len(proyectiles_powerup)>90:
            powerup=False
            proyectiles_powerup=[]

        #player=finalizar(player,asteroides)
        pygame.display.flip()
        borrar_asteroide(asteroides,player)
        if player['vida']==0:
            game_state=menu_perdida(screen)

        if player['points']>=100:
            game_state=menu_victoria_final(screen)
            if game_state==GAME_STATE_PLAYING_2:
                player=create_player2()
                asteroides=[]
                proyectiles=[]
                proyectiles_powerup=[]
                iconoPU=[]
                powerup=False
                cont=25
                fallos=10
                cont_poweup=100

        print(len(proyectiles_powerup))
    return game_state
    


def main():
    pygame.init()
    pygame.mixer.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X,WINDOW_SIZE_Y])

    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=menu(screen)
        elif game_state==GAME_STATE_PLAYING:
            game_state=nivel1(screen,game_state)
        elif game_state==GAME_STATE_PLAYING_2:
            game_state=nivel2(screen,game_state)
        

    pygame.quit()

main()